-- migrate:up
CREATE TABLE IF NOT EXISTS public."transaction" (
    id SERIAL PRIMARY KEY,
    membership_id INT REFERENCES membership(id),
    amount INT not null,
    trrefn varchar(255) not null,
    created_at timestamptz NULL,
    updated_at timestamptz NULL,
    deleted_at timestamptz NULL
);

-- migrate:down
DROP TABLE IF EXISTS public."transaction";
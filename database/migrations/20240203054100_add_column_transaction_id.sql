-- migrate:up
alter table "order" add column if not exists transaction_id INT REFERENCES transaction(id)

-- migrate:down
alter table "order"  drop column if exists transaction_id

-- migrate:up
CREATE TABLE IF NOT EXISTS public."order" (
    id SERIAL PRIMARY KEY,
    product_id INT REFERENCES product(id),
    quantity INT not null,
    amount INT not null,
    created_at timestamptz NULL,
    updated_at timestamptz NULL,
    deleted_at timestamptz NULL
);

-- migrate:down
DROP TABLE IF EXISTS public."order";

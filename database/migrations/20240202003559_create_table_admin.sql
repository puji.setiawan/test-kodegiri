-- migrate:up
CREATE TABLE IF NOT EXISTS public.admin (
	id serial4 NOT NULL,
	name varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	"password" varchar(100) NOT NULL,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	deleted_at timestamptz NULL,
	CONSTRAINT admin_pkey PRIMARY KEY (id)
);

-- migrate:down
DROP TABLE IF EXISTS admin;


-- migrate:up
CREATE TABLE IF NOT EXISTS public."tier" (
    id SERIAL PRIMARY KEY,
    name varchar(255) not null,
    min_poin INT not null,
    max_poin INT not null,
    created_at timestamptz NULL,
    updated_at timestamptz NULL,
    deleted_at timestamptz NULL
);

-- migrate:down
DROP TABLE IF EXISTS public."tier";
-- migrate:up
CREATE TABLE IF NOT EXISTS public.product (
	id serial4 NOT NULL,
	name varchar(255) NOT NULL,
	price int NOT NULL,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	deleted_at timestamptz NULL,
	CONSTRAINT product_pkey PRIMARY KEY (id)
);

-- migrate:down
DROP TABLE IF EXISTS product;


-- migrate:up
CREATE TABLE IF NOT EXISTS public."loyality_program" (
    id SERIAL PRIMARY KEY,
    name varchar(100) NOT NULL,
    tier_id INT REFERENCES tier(id),
    poin INT not null,
    policy varchar(100) not null,
    started_at timestamptz NULL,
    ended_at timestamptz NULL
);

-- migrate:down
DROP TABLE IF EXISTS public."loyality_program";
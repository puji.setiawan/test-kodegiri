-- migrate:up
CREATE TABLE IF NOT EXISTS public.membership (
	id serial4 NOT NULL,
	name varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	phone_number varchar(20) NOT NULL,
	dob varchar(20) NULL,
	address varchar(255) NULL,
	referral varchar(100) NULL,
	earned_point int4 NULL,
	reedemed_point int4 NULL,
	status bool NULL DEFAULT false,
	joined_at timestamptz NULL,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	deleted_at timestamptz NULL,
	CONSTRAINT membership_pkey PRIMARY KEY (id)
);

-- migrate:down
DROP TABLE IF EXISTS membership;


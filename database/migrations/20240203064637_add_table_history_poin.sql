-- migrate:up
CREATE TABLE IF NOT EXISTS public."history_poin" (
    id SERIAL PRIMARY KEY,
    transaction_id varchar(100) not null,
    membership_id INT REFERENCES membership(id),
    loyality_program_id INT REFERENCES loyality_program(id),
    poin_type INT not null,
    type varchar not null,
    created_at timestamptz NULL,
    updated_at timestamptz NULL,
    deleted_at timestamptz NULL
);

-- migrate:down
DROP TABLE IF EXISTS public."history_poin";
package messege

import "github.com/pkg/errors"

var (
	ErrUnauthorized     = errors.New("unauthorized")
	ErrTokenExpired     = errors.New("token has expired")
	ErrInvalidSignature = errors.New("signature is invalid")
	ErrInvalidTokenType = errors.New("invalid token type")
	ErrTokenIsNotBearer = errors.New("token type mush bearer")
	ErrParseToken       = errors.New("failed to parse token")

	ErrUserNotFound    = errors.New("user tidak ditemukan")
	ErrInvalidPassword = errors.New("password salah")
	ErrMemberNotFound  = errors.New("membership tidak ditemukan")
)

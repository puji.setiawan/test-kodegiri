package logger

import (
	"context"
	"log"
	"net/http"
	"net/http/httputil"

	"github.com/sirupsen/logrus"
)

var banner = `


________                           ________                 ________              _____                           
___  __ \_____ _____  _______ _    ___  __/_____________    ___  __ )___  ___________(_)__________________________
__  /_/ /  __  /_  / / /  __  /    __  /_ _  __ \_  ___/    __  __  |  / / /_  ___/_  /__  __ \  _ \_  ___/_  ___/
_  _, _// /_/ /_  /_/ // /_/ /     _  __/ / /_/ /  /        _  /_/ // /_/ /_(__  )_  / _  / / /  __/(__  )_(__  ) 
/_/ |_| \__,_/ _\__, / \__,_/      /_/    \____//_/         /_____/ \__,_/ /____/ /_/  /_/ /_/\___//____/ /____/  
               /____/


`

var Log Logger

func SetLogger(log Logger) {
	Log = log
}

func LogBanner() {
	log.Print(banner)
}

type Logger interface {
	Errorf(format string, args ...interface{})
	Fatalf(format string, args ...interface{})
	Fatal(args ...interface{})
	Infof(format string, args ...interface{})
	Info(args ...interface{})
	Warnf(format string, args ...interface{})
	Debugf(format string, args ...interface{})
	Debug(args ...interface{})
	WithContext(ctx context.Context) EntryLogger
}

type EntryLogger interface {
	Errorf(format string, args ...interface{})
	Fatalf(format string, args ...interface{})
	Fatal(args ...interface{})
	Infof(format string, args ...interface{})
	Info(args ...interface{})
	Warnf(format string, args ...interface{})
	Debugf(format string, args ...interface{})
	Debug(args ...interface{})
}

func New(ctx context.Context) EntryLogger {
	return Log.WithContext(ctx)
}

func LogExternalApi(req *http.Request, res *http.Response) {
	bodyReq, _ := httputil.DumpRequest(req, true)
	bodyRes, _ := httputil.DumpResponse(res, true)
	logrus.WithFields(logrus.Fields{
		"path":     req.URL.String(),
		"method":   req.Method,
		"status":   res.StatusCode,
		"request":  string(bodyReq),
		"response": string(bodyRes),
	}).Info("External API Response: ")
	logrus.WithFields(logrus.Fields{
		"response": req.URL.String(),
	}).Debug("External API Response Body: ")
}

func Warning(message ...any) {
	logrus.Warningln(message...)
}

package usercontext

import (
	"context"
)

type Key string

const UserDataCtxKey Key = "userContext"

type UserData struct {
	ID int `json:"id"`
}

func GetUserIdFromContext(ctx context.Context) int {
	value, ok := ctx.Value(string(UserDataCtxKey)).(*UserData)
	if !ok {
		return 0
	}
	return value.ID
}

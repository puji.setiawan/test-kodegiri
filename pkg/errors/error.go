package errors

import (
	"errors"

	"github.com/sirupsen/logrus"
)

type ErrorType uint8

const (
	Default ErrorType = iota
	TypeBadRequest
	TypeUnauthorized
	TypeNotFound
	TypeConflict
	TypeInternalServerError
	TypeUnprocessableEntity
)

type Error struct {
	*errorLog

	Type ErrorType
	Errs error
}

func newAppErr(pkg, throwerFn, causerFn string, errType ErrorType, err error) *Error {
	errLog := &errorLog{pkg, throwerFn, causerFn}
	return &Error{errLog, errType, err}
}

func (e *Error) Error() string {
	return e.Errs.Error()
}

func HandlerError(err error) *Error {
	var appErr *Error
	switch {
	case errors.As(err, &appErr):
		appErr = newAppErr("handler", "", "", err.(*Error).Type, err)
	default:
		appErr = newAppErr("handler", "", "", Default, err)
	}
	return appErr
}

func ServiceError(throwerFn, causerFn string, errType ErrorType, err error) *Error {

	appErr := newAppErr("service", throwerFn, causerFn, errType, err)
	appErr.logError()

	return appErr
}

func MiddlewareError(throwerFn, causerFn string, err error) *Error {
	appErr := newAppErr("middleware", throwerFn, causerFn, Default, err)
	appErr.logError()
	return appErr
}

type errorLog struct {
	pkg       string
	throwerFn string
	causersFn string
}

func (e *Error) logError() {
	logrus.WithFields(logrus.Fields{
		"Function":  e.errorLog.throwerFn,
		"Caused By": e.errorLog.causersFn,
	}).Errorln(e.Errs.Error())
}

func LogError(throwerFn, causerFn string, errType ErrorType, err error) {
	appErr := newAppErr("LogError", throwerFn, causerFn, errType, err)
	appErr.logError()
}

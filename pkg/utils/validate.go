package utils

import (
	"log"
	"time"
)

func IsEqual(date1String string, date2String string) bool {
	// Define the date format
	dateFormat := "02/01/2006"

	// Parse the date strings into time.Time objects
	date1, err1 := time.Parse(dateFormat, date1String)
	date2, err2 := time.Parse(dateFormat, date2String)

	// Check for parsing errors
	if err1 != nil || err2 != nil {
		log.Println("Error parsing date strings:", err1, err2)
		return false
	}

	// Compare the dates
	if date1.Equal(date2) {
		return true
	}

	return false
}

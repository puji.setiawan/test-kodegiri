package validation

import (
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
)

func (v *ValidatorImpl) registerCustomValidators() {
	_ = v.validate.RegisterValidation("date", v.validDate)
	_ = v.validate.RegisterValidation("name", v.validName)
	_ = v.validate.RegisterValidation("initialBalance", v.validInitialBalance)
	_ = v.validate.RegisterValidation("notOnlySpace", v.notOnlySpace)
	_ = v.validate.RegisterValidation("validDateFormat", v.validDateFormat)

	// add register validation here
}

func (v *ValidatorImpl) validDate(fl validator.FieldLevel) bool {
	dateStr := fl.Field().String()
	_, err := time.Parse("02/01/2006", dateStr)
	return err == nil
}

func (v *ValidatorImpl) validName(fl validator.FieldLevel) bool {
	businessName := fl.Field().String()
	businessName = strings.Trim(businessName, " ")
	if businessName == "" {
		return false
	}
	stringToAccept, err := regexp.Compile(`[a-zA-Z'.,\-() ]+`)
	if err != nil {
		log.Println(err)
		return false
	}
	stringToReject, err := regexp.Compile(`^[!@#$%^&*()_+]`)
	if err != nil {
		log.Println(err)
		return false
	}
	return stringToAccept.MatchString(businessName) && !stringToReject.MatchString(businessName)
}

func (v *ValidatorImpl) validInitialBalance(fl validator.FieldLevel) bool {
	initialBalance := fl.Field().Int()
	strBalance := strconv.Itoa(int(initialBalance))
	return len(strBalance) >= 1 && len(strBalance) <= 13
}

func (v *ValidatorImpl) notOnlySpace(fl validator.FieldLevel) bool {
	sakuName := fl.Field().String()
	sakuName = strings.Trim(sakuName, " ")
	return sakuName != ""
}

func (v *ValidatorImpl) validDateFormat(fl validator.FieldLevel) bool {
	dob := fl.Field().String()
	layout := "2006-01-02"

	_, err := time.Parse(layout, dob)
	return err == nil
}

package validation

import (
	"errors"
	"fmt"

	"github.com/go-playground/validator/v10"
)

func MakeErrors(err validator.ValidationErrors) error {
	var validationErrors validator.ValidationErrors
	if errors.As(err, &validationErrors) {
		for _, fe := range validationErrors {
			return getCustomErrMessageByTag(fe, err)
		}
	}
	return err
}

func getCustomErrMessageByTag(fe validator.FieldError, err error) error {
	switch fe.Tag() {
	case "oneof",
		"email",
		"validDateFormat":
		return fmt.Errorf("%s tidak sesuai", fe.Field())
	case "required":
		return fmt.Errorf("%s harus diisi", fe.Field())

	// add tag for custom error here

	default:
		return err
	}
}

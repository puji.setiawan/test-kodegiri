package jwt

import (
	"fmt"
	"strconv"
	"time"

	"boilerplate_repository/config"
	"boilerplate_repository/pkg/messege"
	usercontext "boilerplate_repository/pkg/user_context"

	"github.com/golang-jwt/jwt"
)

type Jwt struct {
	accessTokenSecret    string
	accessTokenDuration  time.Duration
	refreshTokenSecret   string
	refreshTokenDuration time.Duration
}

func New(cfg *config.Config) *Jwt {
	return &Jwt{
		accessTokenSecret:    cfg.AccessTokenSecret,
		accessTokenDuration:  cfg.AccessTokenDuration,
		refreshTokenSecret:   cfg.RefreshTokenSecret,
		refreshTokenDuration: cfg.RefreshTokenDuration,
	}
}

type Token struct {
	AccessToken           string
	AccessTokenExpiredAt  string
	RefreshToken          string
	RefreshTokenExpiredAt string
}

type JwtRequest struct {
	UserId      string
	PhoneNumber string
	DeviceId    string
}

type JwtLivelinessRequest struct {
	JwtRequest
	PassedAt       time.Time
	LivelinessData string
}

func (j *Jwt) GenerateToken(req JwtRequest) (*Token, error) {
	accessToken, err := j.generateAccessToken(req)
	if err != nil {
		return nil, err
	}
	refreshToken, err := j.generateRefreshToken(req)
	if err != nil {
		return nil, err
	}
	return &Token{
		AccessToken:           accessToken,
		AccessTokenExpiredAt:  stringDuration(j.accessTokenDuration),
		RefreshToken:          refreshToken,
		RefreshTokenExpiredAt: stringDuration(j.refreshTokenDuration),
	}, nil
}

func (j *Jwt) generateAccessToken(req JwtRequest) (string, error) {
	accessToken, err := j.generateToken(req, j.accessTokenSecret, j.accessTokenDuration)
	if err != nil {
		return "", err
	}
	return accessToken, nil
}

func (j *Jwt) generateRefreshToken(req JwtRequest) (string, error) {
	refreshToken, err := j.generateToken(req, j.refreshTokenSecret, j.refreshTokenDuration)
	if err != nil {
		return "", err
	}
	return refreshToken, nil
}

type Claims struct {
	UserID string `json:"userId"`
	jwt.StandardClaims
}

type RayaClaims struct {
	UserID      string `json:"userId"`
	PhoneNumber string `json:"phoneNumber"`
	jwt.StandardClaims
}

func (j *Jwt) generateToken(req JwtRequest, secret string, duration time.Duration) (string, error) {
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["userId"] = req.UserId
	atClaims["phoneNumber"] = req.PhoneNumber
	atClaims["deviceId"] = req.DeviceId
	atClaims["exp"] = time.Now().Add(duration).Unix()
	atClaims["timestamp"] = time.Now().UnixNano()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	signedToken, err := token.SignedString([]byte(secret))
	if err != nil {
		return "", err
	}
	return signedToken, nil
}

func stringDuration(duration time.Duration) string {
	return time.Now().Add(duration).Format("2006-01-02 15:04:05")
}

func (j *Jwt) ValidateTokenUnverified(token string) (*usercontext.UserData, error) {
	unverified, _, err := new(jwt.Parser).ParseUnverified(token, &RayaClaims{})
	if err != nil {
		return nil, err
	}
	if claims, ok := unverified.Claims.(*RayaClaims); ok {
		intUserId, _ := strconv.Atoi(fmt.Sprintf("%v", claims.UserID))
		return &usercontext.UserData{
			ID: intUserId,
		}, nil
	}
	return nil, messege.ErrParseToken
}

func (j *Jwt) ValidateAccessToken(tokenString string) (*usercontext.UserData, error) {
	token, err := jwt.ParseWithClaims(tokenString, &Claims{}, func(token *jwt.Token) (any, error) {
		return []byte(j.accessTokenSecret), nil
	})
	if err != nil {
		if ve, ok := err.(*jwt.ValidationError); ok && ve.Errors == jwt.ValidationErrorExpired {
			return nil, messege.ErrTokenExpired
		}
		return nil, messege.ErrInvalidSignature
	}
	if !token.Valid {
		return nil, messege.ErrUnauthorized
	}
	if claims, ok := token.Claims.(*Claims); ok && token.Valid {
		intUserId, _ := strconv.Atoi(fmt.Sprintf("%v", claims.UserID))
		return &usercontext.UserData{
			ID: intUserId,
		}, nil
	}
	return nil, messege.ErrParseToken
}

func (j *Jwt) ValidateRefreshToken(authToken string) (*JwtRequest, error) {
	token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(j.refreshTokenSecret), nil
	})
	if err != nil {
		return nil, fmt.Errorf("failed to parse token: %v", err)
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		userID, ok := claims["userId"].(string)
		if !ok {
			return nil, fmt.Errorf("invalid userId format in claims")
		}
		phoneNumber, ok := claims["phoneNumber"].(string)
		if !ok {
			return nil, fmt.Errorf("invalid phoneNumber format in claims")
		}
		deviceID, ok := claims["deviceId"].(string)
		if !ok {
			return nil, fmt.Errorf("invalid deviceId format in claims")
		}
		return &JwtRequest{
			UserId:      userID,
			PhoneNumber: phoneNumber,
			DeviceId:    deviceID,
		}, nil
	}
	return nil, fmt.Errorf("token validation failed")
}

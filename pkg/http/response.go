package http

import (
	"net/http"
	"time"

	apperr "boilerplate_repository/pkg/errors"

	"github.com/gin-gonic/gin"
)

type response struct {
	Code       StatusCode `json:"code,omitempty"`
	Status     StatusText `json:"status,omitempty"`
	Data       any        `json:"data,omitempty"`
	Errors     string     `json:"errors,omitempty"`
	ServerTime int64      `json:"serverTime,omitempty"`
}

func ResponseSuccess(ctx *gin.Context, data any) {
	ctx.JSON(http.StatusOK, &response{
		Code:       CodeSuccess,
		Status:     StatusSuccess,
		Data:       data,
		ServerTime: time.Now().Unix(),
	})
}

func ResponseBadRequest(ctx *gin.Context, err error) {
	Error(ctx, http.StatusBadRequest, err)
}

func ResponseUnauthorized(ctx *gin.Context, err error) {
	Error(ctx, http.StatusUnauthorized, err)
}

func ResponseNotFound(ctx *gin.Context, err error) {
	Error(ctx, http.StatusNotFound, err)
}

func ResponseConflict(ctx *gin.Context, err error) {
	Error(ctx, http.StatusConflict, err)
}

func ResponseInternalServerError(ctx *gin.Context, err error) {
	Error(ctx, http.StatusInternalServerError, err)
}

func ResponseStatusUnprocessableEntityError(ctx *gin.Context, err error) {
	Error(ctx, http.StatusUnprocessableEntity, err)
}

func ResponseError(ctx *gin.Context, err error) {
	appErr := err.(*apperr.Error)
	switch appErr.Type {
	case apperr.TypeBadRequest:
		ResponseBadRequest(ctx, err)
	case apperr.TypeUnauthorized:
		ResponseUnauthorized(ctx, err)
	case apperr.TypeNotFound:
		ResponseNotFound(ctx, err)
	case apperr.TypeConflict:
		ResponseConflict(ctx, err)
	case apperr.TypeInternalServerError:
		ResponseInternalServerError(ctx, err)
	case apperr.TypeUnprocessableEntity:
		ResponseStatusUnprocessableEntityError(ctx, err)
	default:
		ResponseInternalServerError(ctx, err)
	}
}

func Error(ctx *gin.Context, httpCode int, err error) {
	ctx.JSON(httpCode, &response{
		Code:       statusCode(httpCode),
		Status:     statusText(httpCode),
		Errors:     err.Error(),
		ServerTime: time.Now().Unix(),
	})
}

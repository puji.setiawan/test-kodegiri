package server

import (
	"boilerplate_repository/config"
	"boilerplate_repository/pkg/logger"
	"boilerplate_repository/server/dependency"

	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload"
)

type Server struct {
	cfg *config.Config
}

func NewHttpServer() *Server {
	server := &Server{}
	server.cfg = config.Get()
	return server
}

func (s *Server) Run() {
	setMode(s.cfg)

	logger.LogBanner()
	router := gin.Default()
	dependency.InitializeRouter(router).Run(s.cfg.Port)
}

func setMode(cfg *config.Config) {
	appEnv := cfg.Environment
	if appEnv == "PROD" {
		gin.SetMode(gin.ReleaseMode)
	}
}

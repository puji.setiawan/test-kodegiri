package router

import (
	"boilerplate_repository/config"
	"boilerplate_repository/internal/handler"
	"boilerplate_repository/server/middleware"
	"log"

	"github.com/fvbock/endless"
	"github.com/gin-gonic/gin"
)

type Router struct {
	router          *gin.Engine
	middleware      *middleware.Auth
	cfg             *config.Config
	authHandler     *handler.AuthHandler
	loyalityHandler *handler.LoyalityManagementHandler
	tierHandler     *handler.TierHandler
}

func NewRouter(
	router *gin.Engine,
	cfg *config.Config,
	middleware *middleware.Auth,
	authHandler *handler.AuthHandler,
	loyalityHandler *handler.LoyalityManagementHandler,
	tierHandler *handler.TierHandler,

) *Router {
	return &Router{
		router:          router,
		middleware:      middleware,
		cfg:             cfg,
		authHandler:     authHandler,
		loyalityHandler: loyalityHandler,
		tierHandler:     tierHandler,
	}
}

func (r *Router) Run(port string) {
	r.setRoutes()
	r.run(port)
}

func (r *Router) setRoutes() {
	v1Router := r.router.Group("/api/v1")
	v1Router.Use(middleware.GinRecoveryMiddleware())

	r.setAuthRoutes(v1Router)
	r.setExampleRoutes(v1Router)
}

// func (r *Router) setSwaggerRoutes() {
// 	r.router.GET("/swagger-ui/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
// }

func (r *Router) setAuthRoutes(rg *gin.RouterGroup) {
	authRoutes := rg.Group("/auth")
	authRoutes.POST("/login", r.authHandler.Login)
	authRoutes.POST("/refresh", r.authHandler.GetRefreshToken)
}

func (r *Router) setExampleRoutes(rg *gin.RouterGroup) {
	loyalityRoutes := rg.Group("/loyality")
	loyalityRoutes.Use(r.middleware.Authenticate())
	loyalityRoutes.GET("/membership", r.loyalityHandler.ListMember)
	loyalityRoutes.GET("/membership/:id", r.loyalityHandler.GetDetailMember)
	loyalityRoutes.POST("/tier", r.tierHandler.CreateTier)
	loyalityRoutes.GET("/tier", r.tierHandler.ListTier)
	loyalityRoutes.PUT("/tier/:id", r.tierHandler.UpdateTier)
	loyalityRoutes.DELETE("/tier/:id", r.tierHandler.DeleteTier)
	loyalityRoutes.POST("/transaction", r.loyalityHandler.Transaction)
	loyalityRoutes.POST("/referral", r.loyalityHandler.Transaction)
	loyalityRoutes.POST("/member-get-member", r.loyalityHandler.MemberGetMember)
	loyalityRoutes.POST("/reedem", r.loyalityHandler.ReedemPoin)

}

func (r *Router) run(port string) {
	if port == "" {
		port = "8080"
	}
	log.Printf("running on port : [%v]", port)
	if err := endless.ListenAndServe(":"+port, r.router); err != nil {
		log.Fatalf("failed to run on port [::%v]", port)
	}
}

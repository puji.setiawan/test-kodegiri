package dependency

import (
	"boilerplate_repository/config"
	"boilerplate_repository/infra/database"
	"boilerplate_repository/internal/handler"
	"boilerplate_repository/internal/repository"
	"boilerplate_repository/pkg/jwt"
	"boilerplate_repository/pkg/validation"

	"boilerplate_repository/internal/service"
	"boilerplate_repository/server/middleware"
	"boilerplate_repository/server/router"

	"github.com/gin-gonic/gin"
)

func InitializeRouter(r *gin.Engine) *router.Router {
	validatorImpl := validation.NewValidator()
	cfg := config.Get()
	jwt := jwt.New(cfg)

	// client add here
	db := database.New(cfg)
	middlewareAuth := middleware.NewAuth(jwt)

	// repo add here
	adminRepo := repository.NewAdminRepository(db)
	membershipRepo := repository.NewMembershipRepository(db)
	tierRepo := repository.NewTierRepository(db)

	// service add here
	authService := service.NewAuthService(adminRepo, jwt)
	loyalityService := service.NewLoyalityManagementService(membershipRepo, tierRepo)
	tierService := service.NewTierService(tierRepo)

	// handler add here
	authHandler := handler.NewAuthHandler(validatorImpl, authService)
	loyalityHandler := handler.NewLoyalityManagementHandler(validatorImpl, loyalityService)
	tierHandler := handler.NewTierHandler(validatorImpl, tierService)

	// only 1 router
	routerRouter := router.NewRouter(
		r,
		cfg,
		middlewareAuth,
		authHandler,
		loyalityHandler,
		tierHandler,
	)

	return routerRouter
}

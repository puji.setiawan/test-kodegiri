# Loyalty Management System

Repository for **Loyalty Management System** backend service.

The API Specification of this project can be seen in ```api``` directory.

## Table of Contents

- [Prerequisite](#prerequisite)
- [How To Run](#how-to-run)

## Prerequisite

1. Go 1.20+
2. GNU Makefile
3. Dbmate

## How to run

### 1. Install project dependencies

```sh
go mod install
```

### 2. Install dbmate

Mac installation

```sh
brew install dbmate
```

or Linux installation

```sh
sudo curl -fsSL -o /usr/local/bin/dbmate https://github.com/amacneil/dbmate/releases/latest/download/dbmate-linux-amd64
sudo chmod +x /usr/local/bin/dbmate
```
Example Project
or using docker image

```sh
docker run --rm -it --network=host -v "$(pwd)/db:/db" ghcr.io/amacneil/dbmate new create_users_table
```

Read the documentation here: https://github.com/amacneil/dbmate

### 3. Migrate up

```sh
make migrate-up
```

### 4. Install ```gosec```

```sh
curl -sfL https://raw.githubusercontent.com/securego/gosec/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v2.15.0
```

### 7. Run go security check

```sh
make security-check
```

### 8. Finally, run the server

```sh
make run
```

### How to create Database Migration

```sh
make migrate-create name=create_table_user
```

### How to check status Database Migration

```sh
make migrate-status
```



package service

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/repository"
	"boilerplate_repository/internal/repository/models.go"
	apperr "boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/messege"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"math"
	"strconv"
	"time"
)

type LoyalityManagementService interface {
	ListMember(ctx context.Context) ([]entity.GetListMembershipRes, error)
	DetailMember(ctx context.Context, id string) (*entity.GetDetailMembershipRes, error)
	Transaction(ctx context.Context, req *entity.CreateTransactionReq) (*entity.CreateTransactionRes, error)
	ReedemPoin(ctx context.Context, req *entity.ReedemPoinReq) (*entity.ReedemPoinRes, error)
	MembergetMember(ctx context.Context, req *entity.MembergetMemberReq) error
}

type LoyalityManagementServiceImpl struct {
	MembershipRepo repository.MembershipRepository
	TierRepo       repository.TierRepository
}

func NewLoyalityManagementService(
	memberRepo repository.MembershipRepository,
	TierRepo repository.TierRepository,

) LoyalityManagementService {
	return &LoyalityManagementServiceImpl{
		MembershipRepo: memberRepo,
		TierRepo:       TierRepo,
	}
}

const (
	fnListMember   = "ListMember"
	fnDetailMember = "DetailMember"
)

func (s *LoyalityManagementServiceImpl) ListMember(ctx context.Context) ([]entity.GetListMembershipRes, error) {
	res := make([]entity.GetListMembershipRes, 0)
	member, err := s.MembershipRepo.GetListMembership(ctx)
	if err != nil {
		return nil, apperr.ServiceError(fnListMember, "MembershipRepo.GetListMembership", apperr.TypeInternalServerError, err)
	}

	for _, v := range *member {
		status := "InActive"
		if v.Status {
			status = "Active"
		}
		res = append(res, entity.GetListMembershipRes{
			MemberNo:      v.Id,
			Name:          v.Name,
			Email:         v.Email,
			PhoneNo:       v.PhoneNumber,
			JoinDate:      v.CreatedAt.Format("01-02-2006"),
			RemainedPoint: v.EarnedPoint,
			Status:        status,
		})
	}

	return res, nil
}

func (s *LoyalityManagementServiceImpl) DetailMember(ctx context.Context, id string) (*entity.GetDetailMembershipRes, error) {
	intId, _ := strconv.Atoi(id)

	member, err := s.MembershipRepo.GetDetailMembership(ctx, intId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, apperr.ServiceError(fnDetailMember, "MembershipRepo.GetDetailMembership", apperr.TypeNotFound, messege.ErrMemberNotFound)
		}
		return nil, apperr.ServiceError(fnDetailMember, "MembershipRepo.GetDetailMembership", apperr.TypeInternalServerError, err)
	}

	poin, err := s.MembershipRepo.GetHistoryPoinByMemberId(ctx, intId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, apperr.ServiceError(fnDetailMember, "MembershipRepo.GetDetailMembership", apperr.TypeNotFound, messege.ErrMemberNotFound)
		}
		return nil, apperr.ServiceError(fnDetailMember, "MembershipRepo.GetDetailMembership", apperr.TypeInternalServerError, err)
	}

	res := entity.GetDetailMembershipRes{
		MemberNo:      member.Id,
		Name:          member.Name,
		Email:         member.Email,
		PhoneNo:       member.PhoneNumber,
		Dob:           member.Dob,
		Address:       member.Address,
		EarnedPoint:   member.EarnedPoint,
		ReedemedPoint: member.ReedemedPoint,
		RemainedPoint: member.EarnedPoint - member.ReedemedPoint,
		JoinDate:      member.JoinedAt.Format("01-02-2006"),
		Status:        "Active",
		HistoryPoint:  []entity.HistoryPoint{},
	}

	for _, v := range *poin {
		res.HistoryPoint = append(res.HistoryPoint, entity.HistoryPoint{
			TrxRef:  v.TransactionId,
			TrxDate: v.CreatedAt.Format("02 Jan 2006"),
			Type:    v.PoinType,
			Point:   v.Poin,
		})
	}

	return &res, nil
}

func (s *LoyalityManagementServiceImpl) Transaction(ctx context.Context, req *entity.CreateTransactionReq) (*entity.CreateTransactionRes, error) {
	now := time.Now()
	productArr := make([]int, len(req.Items))
	mapProduct := make(map[int]int)
	data := make([]models.OrderSchema, 0)
	totalAmount := 0

	member, err := s.MembershipRepo.GetDetailMembership(ctx, req.MemberId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, apperr.ServiceError(fnDetailMember, "MembershipRepo.GetDetailMembership", apperr.TypeNotFound, messege.ErrMemberNotFound)
		}
		return nil, apperr.ServiceError(fnDetailMember, "MembershipRepo.GetDetailMembership", apperr.TypeInternalServerError, err)
	}

	for i, v := range req.Items {
		productArr[i] = v.Id
		mapProduct[v.Id] = v.Quantity
	}

	product, err := s.MembershipRepo.GetProductById(ctx, productArr)
	if err != nil {
		return nil, apperr.ServiceError(fnDetailMember, "MembershipRepo.GetProductById", apperr.TypeInternalServerError, err)
	}

	for _, v := range product {
		qty := mapProduct[v.Id]
		amount := v.Price * qty
		totalAmount += amount

		data = append(data, models.OrderSchema{
			ProductId:     v.Id,
			Quantity:      qty,
			Amount:        amount,
			CreatedAt:     now,
			TransactionId: 3,
		})
	}

	lastTrx, err := s.MembershipRepo.GetLastTrx(ctx)
	if err != nil {
		return nil, apperr.ServiceError(fnDetailMember, "MembershipRepo.GetLastTrx", apperr.TypeInternalServerError, err)
	}

	trrefn := fmt.Sprintf("TRINV/%d/%d%d%d", lastTrx.Id+1, now.Day(), now.Month(), now.Year())

	trx, err := s.MembershipRepo.CreateTransaction(ctx, &models.TransactionSchema{
		MembershipId: member.Id,
		Amount:       totalAmount,
		Trrefn:       trrefn,
		CreatedAt:    time.Now(),
	})
	if err != nil {
		return nil, apperr.ServiceError(fnDetailMember, "MembershipRepo.CreateTransaction", apperr.TypeInternalServerError, err)
	}

	for i := range data {
		data[i].TransactionId = trx.Id
	}

	err = s.MembershipRepo.CreateOrder(ctx, data)
	if err != nil {
		return nil, apperr.ServiceError(fnDetailMember, "MembershipRepo.CreateOrder", apperr.TypeInternalServerError, err)
	}

	go s.getTransaksionalPoin(trx)

	return &entity.CreateTransactionRes{
		Trrefn:          trrefn,
		TotalAmount:     totalAmount,
		TransactionDate: now.Format("02 Jan 2006"),
	}, nil
}

func (s *LoyalityManagementServiceImpl) getTransaksionalPoin(trx *models.TransactionSchema) {
	ctx := context.Background()
	member, err := s.MembershipRepo.GetDetailMembership(ctx, trx.MembershipId)
	if err != nil {
		log.Println("failed to GetDetailMembership with err : ", err.Error())
	}

	tierId := s.getTierByPoin(ctx, member.EarnedPoint-member.ReedemedPoint)
	if tierId == 0 {
		log.Println("not have tier")
		return
	}
	program, err := s.MembershipRepo.GetLoyalityProgram(ctx, tierId, "transaksional")
	if err != nil {
		log.Println("failed to GetLoyalityProgram with err : ", err.Error())
		return
	}

	multiples := math.Floor(float64(trx.Amount) / float64(program.Multiples))

	err = s.MembershipRepo.CreateHistoryPoin(ctx, &models.HistoryPoinSchema{
		TransactionId:     trx.Trrefn,
		MembershipId:      trx.MembershipId,
		LoyalityProgramId: program.Id,
		Poin:              program.Poin * int(multiples),
		PoinType:          "earned",
		CreatedAt:         time.Now(),
	})
	if err != nil {
		log.Println("failed to CreateHistoryPoin with err : ", err.Error())
		return
	}

	member.EarnedPoint = member.EarnedPoint + (program.Poin * int(multiples))
	err = s.MembershipRepo.UpdateMember(ctx, member)
	if err != nil {
		log.Println("failed to UpdateMember with err : ", err.Error())
		return
	}
}

func (s *LoyalityManagementServiceImpl) getTierByPoin(ctx context.Context, poin int) int {
	tier, err := s.TierRepo.ListTier(ctx)
	if err != nil {
		log.Println("failed to ListTier with err : ", err.Error())
	}

	for _, v := range tier {
		if poin > v.MinPoin && poin < v.MaxPoin {
			return v.Id
		}
	}

	return 0
}

func (s *LoyalityManagementServiceImpl) ReedemPoin(ctx context.Context, req *entity.ReedemPoinReq) (*entity.ReedemPoinRes, error) {
	member, err := s.MembershipRepo.GetDetailMembership(ctx, req.MemberId)
	if err != nil {
		log.Println("failed to GetDetailMembership with err : ", err.Error())
	}

	err = s.MembershipRepo.CreateHistoryPoin(ctx, &models.HistoryPoinSchema{
		TransactionId:     "RDM-XXXXXXX",
		MembershipId:      req.MemberId,
		LoyalityProgramId: 1,
		Poin:              req.TotalPoin,
		PoinType:          "reedemed",
		CreatedAt:         time.Now(),
	})
	if err != nil {
		return nil, apperr.ServiceError(fnCreateTier, "TierRepo.CreateTier", apperr.TypeInternalServerError, err)
	}

	member.EarnedPoint = member.EarnedPoint - req.TotalPoin
	member.ReedemedPoint = member.ReedemedPoint + req.TotalPoin

	err = s.MembershipRepo.UpdateMember(ctx, member)
	if err != nil {
		return nil, apperr.ServiceError(fnListMember, "MembershipRepo.GetListMembership", apperr.TypeInternalServerError, err)
	}

	return &entity.ReedemPoinRes{
		MemberId:      member.Id,
		EarnedPoin:    member.EarnedPoint,
		ReedemedPoin:  member.ReedemedPoint,
		RemainingPoin: member.EarnedPoint - member.ReedemedPoint,
		ReedemedAt:    time.Now().Format("02 Jan 2006"),
	}, nil
}

func (s *LoyalityManagementServiceImpl) MembergetMember(ctx context.Context, req *entity.MembergetMemberReq) error {
	member, err := s.MembershipRepo.GetDetailMembership(ctx, req.MemberId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return apperr.ServiceError(fnDetailMember, "MembershipRepo.GetDetailMembership", apperr.TypeNotFound, messege.ErrMemberNotFound)
		}
		log.Println("failed to GetDetailMembership with err : ", err.Error())
	}

	newMember := make([]models.MembershipSchema, 0)

	for _, v := range req.NewMember {
		newMember = append(newMember, models.MembershipSchema{
			Name:        v.Name,
			Email:       v.Email,
			PhoneNumber: v.PhoneNo,
		})
	}

	err = s.MembershipRepo.InsertMember(ctx, newMember)
	if err != nil {
		return apperr.ServiceError(fnListMember, "MembershipRepo.GetListMembership", apperr.TypeInternalServerError, err)
	}

	go s.getReferralPoin(member, len(newMember))

	return nil
}

func (s *LoyalityManagementServiceImpl) getReferralPoin(member *models.MembershipSchema, newMemberCount int) {
	ctx := context.Background()
	now := time.Now()

	tierId := s.getTierByPoin(ctx, member.EarnedPoint-member.ReedemedPoint)
	if tierId == 0 {
		log.Println("not have tier")
		return
	}
	program, err := s.MembershipRepo.GetLoyalityProgram(ctx, tierId, "referral")
	if err != nil {
		log.Println("failed to GetLoyalityProgram with err : ", err.Error())
		return
	}

	multiples := newMemberCount / program.Multiples
	err = s.MembershipRepo.CreateHistoryPoin(ctx, &models.HistoryPoinSchema{
		TransactionId:     fmt.Sprintf("TRMGM/%d/%d%d%d", 1, now.Day(), now.Month(), now.Year()),
		MembershipId:      member.Id,
		LoyalityProgramId: program.Id,
		Poin:              program.Poin * multiples,
		PoinType:          "earned",
		CreatedAt:         now,
	})
	if err != nil {
		log.Println("failed to CreateHistoryPoin with err : ", err.Error())
		return
	}

	member.EarnedPoint = member.EarnedPoint + (program.Poin * multiples)
	err = s.MembershipRepo.UpdateMember(ctx, member)
	if err != nil {
		log.Println("failed to UpdateMember with err : ", err.Error())
		return
	}

}

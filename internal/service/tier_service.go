package service

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/repository"
	"boilerplate_repository/internal/repository/models.go"
	apperr "boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/messege"
	"context"
	"database/sql"
	"errors"
	"strconv"
	"time"
)

type TierService interface {
	CreateTier(ctx context.Context, req *entity.CreateTierReq) error
	ListTier(ctx context.Context) ([]*models.TierSchema, error)
	UpdateTier(ctx context.Context, req *entity.CreateTierReq, id string) error
	DeleteTier(ctx context.Context, id string) error
}

type TierServiceImpl struct {
	TierRepo repository.TierRepository
}

func NewTierService(
	TierRepo repository.TierRepository,
) TierService {
	return &TierServiceImpl{
		TierRepo: TierRepo,
	}
}

const (
	fnCreateTier = "CreateTier"
)

func (s *TierServiceImpl) CreateTier(ctx context.Context, req *entity.CreateTierReq) error {
	err := s.TierRepo.CreateTier(ctx, models.TierSchema{
		Name:      req.Name,
		MinPoin:   req.MinPoin,
		MaxPoin:   req.MaxPoin,
		CreatedAt: time.Now(),
	})
	if err != nil {
		return apperr.ServiceError(fnCreateTier, "TierRepo.CreateTier", apperr.TypeInternalServerError, err)
	}
	return nil
}

func (s *TierServiceImpl) ListTier(ctx context.Context) ([]*models.TierSchema, error) {
	list, err := s.TierRepo.ListTier(ctx)
	if err != nil {
		return nil, apperr.ServiceError(fnCreateTier, "TierRepo.CreateTier", apperr.TypeInternalServerError, err)
	}
	return list, nil
}

func (s *TierServiceImpl) UpdateTier(ctx context.Context, req *entity.CreateTierReq, id string) error {
	idInt, _ := strconv.Atoi(id)

	tier, err := s.TierRepo.GetTierById(ctx, idInt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return apperr.ServiceError(fnDetailMember, "MembershipRepo.GetListMembership", apperr.TypeNotFound, messege.ErrMemberNotFound)
		}
		return apperr.ServiceError(fnCreateTier, "TierRepo.CreateTier", apperr.TypeInternalServerError, err)
	}

	tier.MaxPoin = req.MaxPoin
	tier.MinPoin = req.MinPoin
	tier.Name = req.Name
	tier.UpdatedAt = time.Now()

	err = s.TierRepo.UpdateTier(ctx, tier)
	if err != nil {
		return apperr.ServiceError(fnCreateTier, "TierRepo.CreateTier", apperr.TypeInternalServerError, err)
	}
	return nil
}

func (s *TierServiceImpl) DeleteTier(ctx context.Context, id string) error {
	idInt, _ := strconv.Atoi(id)

	tier, err := s.TierRepo.GetTierById(ctx, idInt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return apperr.ServiceError(fnDetailMember, "MembershipRepo.GetListMembership", apperr.TypeNotFound, messege.ErrMemberNotFound)
		}
		return apperr.ServiceError(fnCreateTier, "TierRepo.CreateTier", apperr.TypeInternalServerError, err)
	}

	err = s.TierRepo.Delete(ctx, tier)
	if err != nil {
		return apperr.ServiceError(fnCreateTier, "TierRepo.CreateTier", apperr.TypeInternalServerError, err)
	}
	return nil
}

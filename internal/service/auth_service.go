package service

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/repository"
	apperr "boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/jwt"
	"boilerplate_repository/pkg/messege"
	"context"
	"database/sql"
	"errors"
	"strconv"
)

type AuthService interface {
	Login(ctx context.Context, req *entity.LoginReq) (*entity.LoginRes, error)
	RefreshToken(ctx context.Context, req *entity.RefreshReq) (any, error)
}

type AuthServiceImpl struct {
	AdminRepo repository.AdminRepository
	Jwt       *jwt.Jwt
}

func NewAuthService(
	AdminRepo repository.AdminRepository,
	Jwt *jwt.Jwt,
) AuthService {
	return &AuthServiceImpl{
		AdminRepo: AdminRepo,
		Jwt:       Jwt,
	}
}

const (
	fnGetAuth       = "GetAuth"
	fnGetPokmonAuth = "GetPokmonAuth"
	fnRefresh       = "Refresh"
)

func (s *AuthServiceImpl) Login(ctx context.Context, req *entity.LoginReq) (*entity.LoginRes, error) {
	user, err := s.AdminRepo.GetAdminByEmail(ctx, req.Email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, apperr.ServiceError(fnGetAuth, "AuthRepo.GetAuth", apperr.TypeNotFound, messege.ErrUserNotFound)
		}
		return nil, apperr.ServiceError(fnGetAuth, "AuthRepo.GetAuth", apperr.TypeInternalServerError, err)
	}

	if user.Password != req.Password {
		return nil, apperr.ServiceError(fnGetAuth, "CheckPassword", apperr.TypeBadRequest, messege.ErrInvalidPassword)
	}

	token, err := s.Jwt.GenerateToken(jwt.JwtRequest{
		UserId:      strconv.Itoa(user.Id),
		PhoneNumber: "212121",
		DeviceId:    "121212",
	})
	if err != nil {
		return nil, apperr.ServiceError(fnGetPokmonAuth, "Jwt.GenerateToken", apperr.TypeInternalServerError, err)
	}

	return &entity.LoginRes{
		UserId:               user.Id,
		AccessToken:          token.AccessToken,
		RefreshToken:         token.RefreshToken,
		AccessTokenExpiredAt: token.AccessTokenExpiredAt,
		RefrehTokenExpiredAt: token.RefreshTokenExpiredAt,
	}, nil
}

func (s *AuthServiceImpl) RefreshToken(ctx context.Context, req *entity.RefreshReq) (any, error) {
	data, err := s.Jwt.ValidateRefreshToken(req.Token)
	if err != nil {
		return nil, apperr.ServiceError(fnRefresh, "s.jwt.ValidateRefreshToken", apperr.TypeInternalServerError, err)
	}

	jwtToken, err := s.Jwt.GenerateToken(*data)
	if err != nil {
		return nil, apperr.ServiceError(fnRefresh, "s.jwt.GenerateToken", apperr.TypeInternalServerError, err)
	}
	return jwtToken, nil
}

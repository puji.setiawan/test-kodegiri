package repository

import (
	"boilerplate_repository/internal/repository/models.go"
	"context"

	"github.com/uptrace/bun"
)

type AdminRepository interface {
	GetAdminByEmail(ctx context.Context, email string) (*models.AdminSchema, error)
}

type AdminRepositoryImpl struct {
	db *bun.DB
}

func NewAdminRepository(db *bun.DB) *AdminRepositoryImpl {
	return &AdminRepositoryImpl{db}
}

func (r *AdminRepositoryImpl) GetAdminByEmail(ctx context.Context, email string) (*models.AdminSchema, error) {
	user := new(models.AdminSchema)
	err := r.db.NewSelect().Model(user).Where("email = ?", email).Scan(ctx)
	if err != nil {
		return nil, err
	}
	return user, nil
}

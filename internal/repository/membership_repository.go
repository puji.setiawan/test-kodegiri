package repository

import (
	"boilerplate_repository/internal/repository/models.go"
	"context"

	"github.com/uptrace/bun"
)

type MembershipRepository interface {
	GetListMembership(ctx context.Context) (*[]models.MembershipSchema, error)
	GetDetailMembership(ctx context.Context, id int) (*models.MembershipSchema, error)
	GetProductById(ctx context.Context, id []int) ([]*models.ProductSchema, error)
	CreateOrder(ctx context.Context, data []models.OrderSchema) error
	CreateTransaction(ctx context.Context, data *models.TransactionSchema) (*models.TransactionSchema, error)
	GetLastTrx(ctx context.Context) (*models.TransactionSchema, error)
	GetLoyalityProgram(ctx context.Context, id int, policy string) (*models.LoyalityProgramSchema, error)
	CreateHistoryPoin(ctx context.Context, data *models.HistoryPoinSchema) error
	GetHistoryPoinByMemberId(ctx context.Context, memberId int) (*[]models.HistoryPoinSchema, error)
	UpdateMember(ctx context.Context, data *models.MembershipSchema) error
	InsertMember(ctx context.Context, data []models.MembershipSchema) error
}

type MembershipRepositoryImpl struct {
	db *bun.DB
}

func NewMembershipRepository(db *bun.DB) *MembershipRepositoryImpl {
	return &MembershipRepositoryImpl{db}
}

func (r *MembershipRepositoryImpl) GetListMembership(ctx context.Context) (*[]models.MembershipSchema, error) {
	member := new([]models.MembershipSchema)
	err := r.db.NewSelect().Model(member).Scan(ctx)
	if err != nil {
		return nil, err
	}
	return member, nil
}

func (r *MembershipRepositoryImpl) GetDetailMembership(ctx context.Context, id int) (*models.MembershipSchema, error) {
	member := new(models.MembershipSchema)
	err := r.db.NewSelect().Model(member).Where("id = ?", id).Scan(ctx)
	if err != nil {
		return nil, err
	}
	return member, nil
}

func (r *MembershipRepositoryImpl) GetProductById(ctx context.Context, id []int) ([]*models.ProductSchema, error) {
	product := []*models.ProductSchema{}
	err := r.db.NewSelect().Model(&product).Where("id IN (?)", bun.In(id)).Scan(ctx)
	if err != nil {
		return nil, err
	}
	return product, nil
}

func (r *MembershipRepositoryImpl) CreateOrder(ctx context.Context, data []models.OrderSchema) error {
	_, err := r.db.NewInsert().Model(&data).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *MembershipRepositoryImpl) CreateTransaction(ctx context.Context, data *models.TransactionSchema) (*models.TransactionSchema, error) {
	_, err := r.db.NewInsert().Model(data).Exec(ctx)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (r *MembershipRepositoryImpl) GetLastTrx(ctx context.Context) (*models.TransactionSchema, error) {
	trx := new(models.TransactionSchema)
	err := r.db.NewSelect().Model(trx).Order("id desc").Limit(1).Scan(ctx)
	if err != nil {
		return nil, err
	}
	return trx, nil
}

func (r *MembershipRepositoryImpl) GetLoyalityProgram(ctx context.Context, id int, policy string) (*models.LoyalityProgramSchema, error) {
	lp := new(models.LoyalityProgramSchema)
	err := r.db.NewSelect().Model(lp).
		Where("tier_id = ?", id).
		Where("policy = ?", policy).
		Scan(ctx)
	if err != nil {
		return nil, err
	}
	return lp, nil
}

func (r *MembershipRepositoryImpl) CreateHistoryPoin(ctx context.Context, data *models.HistoryPoinSchema) error {
	_, err := r.db.NewInsert().Model(data).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *MembershipRepositoryImpl) GetHistoryPoinByMemberId(ctx context.Context, memberId int) (*[]models.HistoryPoinSchema, error) {
	member := new([]models.HistoryPoinSchema)
	err := r.db.NewSelect().Model(member).Where("membership_id = ?", memberId).Order("id desc").Scan(ctx)
	if err != nil {
		return nil, err
	}
	return member, nil
}

func (r *MembershipRepositoryImpl) UpdateMember(ctx context.Context, data *models.MembershipSchema) error {
	_, err := r.db.NewUpdate().Model(data).WherePK().Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *MembershipRepositoryImpl) InsertMember(ctx context.Context, data []models.MembershipSchema) error {
	_, err := r.db.NewInsert().Model(&data).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

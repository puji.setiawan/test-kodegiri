package repository

import (
	"boilerplate_repository/internal/repository/models.go"
	"context"

	"github.com/uptrace/bun"
)

type TierRepository interface {
	CreateTier(ctx context.Context, data models.TierSchema) error
	Delete(ctx context.Context, data *models.TierSchema) error
	UpdateTier(ctx context.Context, data *models.TierSchema) error
	ListTier(ctx context.Context) ([]*models.TierSchema, error)
	GetTierById(ctx context.Context, id int) (*models.TierSchema, error)
}

type TierRepositoryImpl struct {
	db *bun.DB
}

func NewTierRepository(db *bun.DB) *TierRepositoryImpl {
	return &TierRepositoryImpl{db}
}

func (r *TierRepositoryImpl) CreateTier(ctx context.Context, data models.TierSchema) error {
	_, err := r.db.NewInsert().Model(&data).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *TierRepositoryImpl) ListTier(ctx context.Context) ([]*models.TierSchema, error) {
	var tier []*models.TierSchema
	err := r.db.NewSelect().Model(&tier).Scan(ctx)
	if err != nil {
		return nil, err
	}
	return tier, nil
}

func (r *TierRepositoryImpl) UpdateTier(ctx context.Context, data *models.TierSchema) error {
	_, err := r.db.NewUpdate().Model(data).WherePK().Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *TierRepositoryImpl) Delete(ctx context.Context, data *models.TierSchema) error {
	_, err := r.db.NewDelete().Where("id = ?", data.Id).Model(data).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *TierRepositoryImpl) GetTierById(ctx context.Context, id int) (*models.TierSchema, error) {
	tier := new(models.TierSchema)
	err := r.db.NewSelect().Model(tier).Where("id = ?", id).Scan(ctx)
	if err != nil {
		return nil, err
	}
	return tier, nil
}

package models

import (
	"time"

	"github.com/uptrace/bun"
)

type TransactionSchema struct {
	bun.BaseModel `bun:"transaction"`
	Id            int `bun:",pk,autoincrement"`
	MembershipId  int
	Amount        int
	Trrefn        string
	CreatedAt     time.Time
	UpdatedAt     time.Time `bun:",nullzero"`
	DeletedAt     time.Time `bun:",nullzero"`
}

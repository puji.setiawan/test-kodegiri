package models

import (
	"time"

	"github.com/uptrace/bun"
)

type MembershipSchema struct {
	bun.BaseModel `bun:"membership"`
	Id            int       `bun:"id,pk,autoincrement"`
	Name          string    `bun:"name"`
	Email         string    `bun:"email"`
	PhoneNumber   string    `bun:"phone_number"`
	Dob           string    `bun:"dob"`
	Address       string    `bun:"address"`
	Referral      string    `bun:"referral"`
	EarnedPoint   int       `bun:"earned_point"`
	ReedemedPoint int       `bun:"reedemed_point"`
	Status        bool      `bun:"status"`
	JoinedAt      time.Time `bun:"joined_at"`
	CreatedAt     time.Time `bun:"created_at"`
	UpdatedAt     time.Time `bun:"updated_at"`
	DeletedAt     time.Time `bun:"deleted_at"`
}

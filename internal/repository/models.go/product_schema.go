package models

import (
	"time"

	"github.com/uptrace/bun"
)

type ProductSchema struct {
	bun.BaseModel `bun:"product"`
	Id            int `bun:",pk,autoincrement"`
	Name          string
	Price         int
	CreatedAt     time.Time
	UpdatedAt     time.Time `bun:",nullzero"`
	DeletedAt     time.Time `bun:",nullzero"`
}

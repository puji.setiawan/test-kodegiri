package models

import (
	"time"

	"github.com/uptrace/bun"
)

type HistoryPoinSchema struct {
	bun.BaseModel     `bun:"history_poin"`
	Id                int `bun:",pk,autoincrement"`
	TransactionId     string
	MembershipId      int
	LoyalityProgramId int
	Poin              int
	PoinType          string
	CreatedAt         time.Time `bun:",nullzero"`
	UpdatedAt         time.Time `bun:",nullzero"`
	DeletedAt         time.Time `bun:",nullzero"`
}

package models

import (
	"time"

	"github.com/uptrace/bun"
)

type AdminSchema struct {
	bun.BaseModel `bun:"admin"`
	Id            int       `bun:"id"`
	Name          string    `bun:"name"`
	Email         string    `bun:"email"`
	Password      string    `bun:"password"`
	CreatedAt     time.Time `bun:"created_at"`
	UpdatedAt     time.Time `bun:"updated_at"`
	DeletedAt     time.Time `bun:"deleted_at"`
}

package models

import (
	"time"

	"github.com/uptrace/bun"
)

type LoyalityProgramSchema struct {
	bun.BaseModel `bun:"loyality_program"`
	Id            int
	Name          string
	TierId        int
	Poin          int
	Policy        string
	StartedAt     time.Time
	EndedAt       time.Time
	Multiples     int
}

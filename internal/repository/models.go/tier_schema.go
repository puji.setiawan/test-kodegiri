package models

import (
	"time"

	"github.com/uptrace/bun"
)

type TierSchema struct {
	bun.BaseModel `bun:"tier"`
	Id            int       `bun:"id,pk,autoincrement"`
	Name          string    `bun:"name"`
	MinPoin       int       `bun:"min_poin"`
	MaxPoin       int       `bun:"max_poin"`
	CreatedAt     time.Time `bun:"created_at"`
	UpdatedAt     time.Time `bun:"updated_at,nullzero"`
	DeletedAt     time.Time `bun:"deleted_at,soft_delete,nullzero"`
}

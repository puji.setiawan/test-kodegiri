package models

import (
	"time"

	"github.com/uptrace/bun"
)

type OrderSchema struct {
	bun.BaseModel `bun:"order"`
	Id            string `bun:",pk,autoincrement"`
	ProductId     int
	TransactionId int
	Quantity      int
	Amount        int
	CreatedAt     time.Time
	UpdatedAt     time.Time `bun:",nullzero"`
	DeletedAt     time.Time `bun:",nullzero"`
}

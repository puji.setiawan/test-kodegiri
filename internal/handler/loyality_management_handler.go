package handler

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/service"
	"boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/http"
	"boilerplate_repository/pkg/validation"

	"github.com/gin-gonic/gin"
)

type LoyalityManagementHandler struct {
	validator                 validation.Validator
	LoyalityManagementService service.LoyalityManagementService
}

func NewLoyalityManagementHandler(validator validation.Validator, LoyalityManagementService service.LoyalityManagementService) *LoyalityManagementHandler {
	return &LoyalityManagementHandler{validator, LoyalityManagementService}
}

// ListMembership: HTTP Handler for List Membership
// @Summary List Membership
// @Description List Membership
// @Tags Loyality Management
// @Accept json
// @Produce json
// @Param Authorization header string false "token" default(123456)
// @Success 200 {object} http.response{data=[]entity.GetListMembershipRes} "Success Response"
// @Failure 400 "Bad Request"
// @Failure 500 "InternalServerError"
// @Router /api/v1/loyality/membership [GET]
// ListMembership
func (h *LoyalityManagementHandler) ListMember(ctx *gin.Context) {
	res, err := h.LoyalityManagementService.ListMember(ctx)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, res)
}

// DetailMembership: HTTP Handler for Detail Membership
// @Summary Detail Membership
// @Description Detail Membership
// @Tags Loyality Management
// @Accept json
// @Produce json
// @Param Authorization header string false "token" default(123456)
// @Param id path string true "id" default(1)
// @Success 200 {object} http.response{data=[]entity.GetDetailMembershipRes} "Success Response"
// @Failure 400 "Bad Request"
// @Failure 500 "InternalServerError"
// @Router /api/v1/loyality/membership/{id} [GET]
//
//	DetailMembership
func (h *LoyalityManagementHandler) GetDetailMember(ctx *gin.Context) {
	id := ctx.Param("id")

	res, err := h.LoyalityManagementService.DetailMember(ctx, id)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, res)
}

// DetailMembership: HTTP Handler for Create Transaction
// @Summary Create Transaction
// @Description Create Transaction
// @Tags Loyality Management
// @Accept json
// @Produce json
// @Param Authorization header string false "token" default(123456)
// @Param request body entity.CreateTransactionReq true "Required login request"
// @Success 200 {object} http.response{data=entity.CreateTransactionRes} "Success Response"
// @Failure 400 "Bad Request"
// @Failure 500 "InternalServerError"
// @Router /api/v1/loyality/transaction [POST]
//
//	CreateTier
func (h *LoyalityManagementHandler) Transaction(ctx *gin.Context) {
	req := new(entity.CreateTransactionReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}
	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}
	res, err := h.LoyalityManagementService.Transaction(ctx, req)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, res)
}

// DetailMembership: HTTP Handler for Reedem Poin
// @Summary Reedem Poin
// @Description Reedem Poin
// @Tags Loyality Management
// @Accept json
// @Produce json
// @Param Authorization header string false "token" default(123456)
// @Param request body entity.ReedemPoinReq true "Required login request"
// @Success 200 {object} http.response{data=entity.ReedemPoinRes} "Success Response"
// @Failure 400 "Bad Request"
// @Failure 500 "InternalServerError"
// @Router /api/v1/loyality/reedem [POST]
//
//	Reedem Poin
func (h *LoyalityManagementHandler) ReedemPoin(ctx *gin.Context) {
	req := new(entity.ReedemPoinReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}
	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}
	res, err := h.LoyalityManagementService.ReedemPoin(ctx, req)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, res)
}

// DetailMembership: HTTP Handler for Member Get Member
// @Summary Member Get Member
// @Description Member Get Member
// @Tags Loyality Management
// @Accept json
// @Produce json
// @Param Authorization header string false "token" default(123456)
// @Param request body entity.MembergetMemberReq true "Required login request"
// @Success 200 {object} http.response{} "Success Response"
// @Failure 400 "Bad Request"
// @Failure 500 "InternalServerError"
// @Router /api/v1/loyality/member-get-member [POST]
//
//	ReedemPoin
func (h *LoyalityManagementHandler) MemberGetMember(ctx *gin.Context) {
	req := new(entity.MembergetMemberReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}
	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}
	err = h.LoyalityManagementService.MembergetMember(ctx, req)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, nil)
}

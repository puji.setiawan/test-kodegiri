package handler

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/service"
	"boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/http"
	"boilerplate_repository/pkg/validation"

	"github.com/gin-gonic/gin"
)

type AuthHandler struct {
	validator   validation.Validator
	AuthService service.AuthService
}

func NewAuthHandler(validator validation.Validator, AuthService service.AuthService) *AuthHandler {
	return &AuthHandler{validator, AuthService}
}

// RejectInvitation: HTTP HandlerLogin
// @Summary Auth Login
// @Description Login for admin
// @Tags Auth
// @Accept json
// @Produce json
// @Param Authorization header string false "Bearer Token" default(123456)
// @Param request body entity.LoginReq true "Required login request"
// @Success 200  {object} http.response{data=[]entity.LoginRes} "Success Response"
// @Failure 400 "Bad Request"
// @Failure 500 "InternalServerError"
// @Router /api/v1/auth/login [POST]
// RejectInvitation
func (h *AuthHandler) Login(ctx *gin.Context) {
	req := new(entity.LoginReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}

	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}
	res, err := h.AuthService.Login(ctx, req)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, res)
}

func (h *AuthHandler) GetRefreshToken(ctx *gin.Context) {
	req := new(entity.RefreshReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}

	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}

	res, err := h.AuthService.RefreshToken(ctx, req)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, res)
}

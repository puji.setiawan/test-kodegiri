package handler

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/service"
	"boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/http"
	"boilerplate_repository/pkg/validation"

	"github.com/gin-gonic/gin"
)

type TierHandler struct {
	validator   validation.Validator
	TierService service.TierService
}

func NewTierHandler(validator validation.Validator, TierService service.TierService) *TierHandler {
	return &TierHandler{validator, TierService}
}

// DetailMembership: HTTP Handler for Create Tier
// @Summary Create Tier
// @Description Create Tier
// @Tags Loyality Management
// @Accept json
// @Produce json
// @Param Authorization header string false "token" default(123456)
// @Param request body entity.CreateTierReq true "Required login request"
// @Success 200 {object} http.response{} "Success Response"
// @Failure 400 "Bad Request"
// @Failure 500 "InternalServerError"
// @Router /api/v1/loyality/tier [POST]
//
//	CreateTier
func (h *TierHandler) CreateTier(ctx *gin.Context) {
	req := new(entity.CreateTierReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}
	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}
	err = h.TierService.CreateTier(ctx, req)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, nil)
}

// DetailMembership: HTTP Handler for List Tier
// @Summary List Tier
// @Description List Tier
// @Tags Loyality Management
// @Accept json
// @Produce json
// @Param Authorization header string false "token" default(123456)
// @Success 200 {object} http.response{data=[]entity.DetailTierReq} "Success Response"
// @Failure 400 "Bad Request"
// @Failure 500 "InternalServerError"
// @Router /api/v1/loyality/tier [GET]
//
//	UpdateTier
func (h *TierHandler) ListTier(ctx *gin.Context) {
	data, err := h.TierService.ListTier(ctx)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, data)
}

// DetailMembership: HTTP Handler for Update Tier
// @Summary Update Tier
// @Description Update Tier
// @Tags Loyality Management
// @Accept json
// @Produce json
// @Param Authorization header string false "token" default(123456)
// @Param id path string true "id" default(1)
// @Param request body entity.CreateTierReq true "Required login request"
// @Success 200 {object} http.response{} "Success Response"
// @Failure 400 "Bad Request"
// @Failure 500 "InternalServerError"
// @Router /api/v1/loyality/tier/{id} [PUT]
//
//	UpdateTier
func (h *TierHandler) UpdateTier(ctx *gin.Context) {
	id := ctx.Param("id")

	req := new(entity.CreateTierReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}
	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}

	err = h.TierService.UpdateTier(ctx, req, id)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, nil)
}

// DetailMembership: HTTP Handler for Delete Tier
// @Summary Delete Tier
// @Description Delete Tier
// @Tags Loyality Management
// @Accept json
// @Produce json
// @Param Authorization header string false "token" default(123456)
// @Param id path string true "id" default(1)
// @Success 200 {object} http.response{} "Success Response"
// @Failure 400 "Bad Request"
// @Failure 500 "InternalServerError"
// @Router /api/v1/loyality/tier/{id} [DELETE]
//
//	UpdateTier
func (h *TierHandler) DeleteTier(ctx *gin.Context) {
	id := ctx.Param("id")

	err := h.TierService.DeleteTier(ctx, id)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, nil)
}

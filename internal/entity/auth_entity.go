package entity

type (
	LoginReq struct {
		Email      string `json:"email" validate:"email"`
		Password   string `json:"password" validate:"required"`
		RememberMe bool   `json:"rememberMe"`
	}

	LoginRes struct {
		UserId               int    `json:"userId"`
		AccessToken          string `json:"accessToken"`
		RefreshToken         string `json:"refreshToken"`
		AccessTokenExpiredAt string `json:"accessTokenExpiredAt"`
		RefrehTokenExpiredAt string `json:"refreshTokenExpiredAt"`
	}

	RefreshReq struct {
		Token string `json:"refreshToken" validate:"required"`
	}
)

package entity

type (
	GetListMembershipRes struct {
		MemberNo      int    `json:"memberNo"`
		Name          string `json:"name"`
		Email         string `json:"email"`
		PhoneNo       string `json:"phoneNo"`
		JoinDate      string `json:"joinDate"`
		RemainedPoint int    `json:"remainedPoint"`
		Status        string `json:"status"`
	}

	GetDetailMembershipRes struct {
		MemberNo      int            `json:"memberNo"`
		Name          string         `json:"name"`
		Email         string         `json:"email"`
		PhoneNo       string         `json:"phoneNo"`
		Dob           string         `json:"dob"`
		Address       string         `json:"address"`
		EarnedPoint   int            `json:"earnedPoint"`
		ReedemedPoint int            `json:"reedemedPoint"`
		RemainedPoint int            `json:"remainedPoint"`
		JoinDate      string         `json:"joinDate"`
		Status        string         `json:"status"`
		HistoryPoint  []HistoryPoint `json:"historyPoint"`
	}

	HistoryPoint struct {
		TrxRef  string `json:"trxRef"`
		TrxDate string `json:"trxDate"`
		Type    string `json:"type"`
		Point   int    `json:"point"`
	}

	CreateTransactionReq struct {
		MemberId int    `json:"memberId" validate:"required"`
		Items    []Item `json:"items" validate:"required"`
	}

	Item struct {
		Id       int `json:"id"`
		Quantity int `json:"quantity"`
	}

	CreateTransactionRes struct {
		Trrefn          string `json:"trrefn"`
		TotalAmount     int    `json:"totalAmount"`
		TransactionDate string `json:"transactionDate"`
	}

	ReedemPoinReq struct {
		MemberId  int `json:"memberId" validate:"required"`
		TotalPoin int `json:"totalPoin" validate:"required"`
	}

	ReedemPoinRes struct {
		MemberId      int    `json:"memberId"`
		EarnedPoin    int    `json:"earnedPoin"`
		ReedemedPoin  int    `json:"reedemedPoin"`
		RemainingPoin int    `json:"remainingPoin"`
		ReedemedAt    string `json:"reedemedAt"`
	}

	MembergetMemberReq struct {
		MemberId  int         `json:"memberId" validate:"required"`
		NewMember []RefMember `json:"refMember" validate:"required"`
	}

	RefMember struct {
		Name    string `json:"name" validate:"required"`
		PhoneNo string `json:"phoneNo" validate:"required"`
		Email   string `json:"email" validate:"required"`
	}
)

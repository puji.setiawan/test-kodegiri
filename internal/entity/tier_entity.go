package entity

type (
	CreateTierReq struct {
		Name    string `json:"name" validate:"required"`
		MinPoin int    `json:"minPoin" validate:"required"`
		MaxPoin int    `json:"maxPoin" validate:"required"`
	}

	DetailTierReq struct {
		Id        string `json:"id"`
		Name      string `json:"name"`
		MinPoin   string `json:"minPoin"`
		MaxPoin   string `json:"maxPoin"`
		CreatedAt string `json:"createdAt"`
		UpdatedAt string `json:"updatedAt"`
		DeletedAt string `json:"deletedAt"`
	}
)
